package com.meep.mobile.android.appname.app.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.meep.mobile.android.appname.app.R
import com.meep.mobile.android.appname.app.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_about.*

class AboutFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onResume() {
        super.onResume()

        activity?.setTitle(R.string.resource_about_title)
    }

    override fun initializeContents(savedInstanceState: Bundle?) {
        super.initializeContents(savedInstanceState)

        ev_about_error_view.visibility = View.GONE
        ev_about_empty_view.visibility = View.GONE
        lv_about_loading_view.visibility = View.GONE
    }
}
