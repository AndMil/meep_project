package com.meep.mobile.android.appname.app.resource.master

const val metroStationZoneId = 378
const val busStationZoneId = 382
const val mopedZoneId = 473
const val bikeZoneId = 412
const val locationCode = 1112
