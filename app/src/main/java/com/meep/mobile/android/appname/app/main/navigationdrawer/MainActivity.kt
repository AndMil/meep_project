package com.meep.mobile.android.appname.app.main.navigationdrawer

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import androidx.annotation.NonNull
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.meep.mobile.android.appname.app.R
import com.meep.mobile.android.appname.app.common.BaseActivity
import com.meep.mobile.android.appname.app.common.model.ResourceState.*
import com.meep.mobile.android.appname.app.resource.master.locationCode
import kotlinx.android.synthetic.main.main_activity.*
import kotlinx.android.synthetic.main.navigation_drawer_main_content.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.lang.ref.WeakReference

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val mainActivityViewModel: MainActivityViewModel by viewModel()
    private var exitSnackBarWeakReference: WeakReference<Snackbar>? = null
    private var listenerWeakReference: WeakReference<GeoLocChangeListener>? = null
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    companion object {
        fun getCallingIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    interface GeoLocChangeListener {
        fun geoLocChanged()
    }

    fun setListenerWeakReference(@NonNull listenerWeakReference: GeoLocChangeListener) {
        this.listenerWeakReference = WeakReference(listenerWeakReference)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        initializeContents(savedInstanceState)
        setupNavigation()
    }

    override fun onBackPressed() {
        if (dl_navigation_drawer_drawer_layout.isDrawerOpen(GravityCompat.START)) {
            dl_navigation_drawer_drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if (supportFragmentManager.backStackEntryCount > 1) {
                supportFragmentManager.popBackStack()
            } else {
                exitSnackBarWeakReference?.let { exitSnackBarWeakReference ->
                    exitSnackBarWeakReference.get()?.let { exitSnackBar ->
                        if (exitSnackBar.isShown) {
                            finish()
                        } else {
                            showExitSnackBar()
                        }
                    } ?: showExitSnackBar()
                } ?: showExitSnackBar()
            }
        }
    }

    private fun initializeContents(savedInstanceState: Bundle?) {
        mainActivityViewModel.getSignOut().observe(this,
                Observer<NavigationDrawerSignOutState> {
                    if (it != null) handleDataState(it)
                }
        )
    }

    private fun handleDataState(navigationDrawerSignOutState: NavigationDrawerSignOutState) {
        // This is half baked, it should be improved
        when (navigationDrawerSignOutState) {
            is Loading -> Timber.w("Loading not implemented")
            is Success -> signOut()
            is Error -> Snackbar.make(fl_navigation_drawer_main_content, navigationDrawerSignOutState.errorBundle.stringId, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun signOut() {}

    private fun showExitSnackBar() {
        val exitSnackBar = Snackbar.make(nv_navigation_drawer_navigation_view, R.string.press_back_again, Snackbar.LENGTH_SHORT)
        exitSnackBarWeakReference = WeakReference(exitSnackBar)
        exitSnackBar.show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == locationCode) {
            val pos = permissions.indexOf(Manifest.permission.ACCESS_FINE_LOCATION)
            if (grantResults[pos] == PackageManager.PERMISSION_GRANTED) {
                listenerWeakReference?.get()?.geoLocChanged()
            }
        }
    }

    private fun setupNavigation() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.mapFragment,
                R.id.aboutFragment
        ), dl_navigation_drawer_drawer_layout)
        val toolbar = findViewById<Toolbar>(R.id.toolbar_main)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        // Handle nav drawer item clicks
        nv_navigation_drawer_navigation_view.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        dl_navigation_drawer_drawer_layout.closeDrawers()

        if (item.itemId == nv_navigation_drawer_navigation_view.checkedItem?.itemId)
            return false

        Handler().postDelayed({
            when (item.itemId) {
                R.id.main_drawer_menu_resources -> {
                        navController.navigate(R.id.mapFragment)
                }
                R.id.main_drawer_menu_about -> {
                    navController.navigate(R.id.aboutFragment)
                }
            }
        }, 250L)
        return true
    }

    override fun onSupportNavigateUp() = Navigation.findNavController(this, R.id.nav_host_fragment).navigateUp()
}