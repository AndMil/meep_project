package com.meep.mobile.android.appname.app.resource.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.meep.mobile.android.appname.app.R
import com.meep.mobile.android.appname.app.resource.viewmodel.ResourceViewModel
import com.meep.mobile.android.appname.model.resource.Resource
import kotlinx.android.synthetic.main.fragment_resource_details.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.*

class ResourceDetailsFragment : BottomSheetDialogFragment() {

    private val resourceViewModel: ResourceViewModel by sharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_resource_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        ev_resource_details_error_view.visibility = View.GONE
        ev_resource_details_empty_view.visibility = View.GONE
        lv_resource_details_loading_view.visibility = View.GONE

        resourceViewModel.getSelectedResource().observe(viewLifecycleOwner,
                Observer<Resource> {
                    this.handleData(it)
                }
        )
    }

    override fun onResume() {
        super.onResume()

        activity?.setTitle(R.string.resource_details_title)
    }

    private fun handleData(data: Resource?) {
        if (data == null) {

        } else {
            // Map data to UI
            tv_resource_details_name.text = data.name.toLowerCase(Locale.getDefault()).capitalize()
            val arrivalText = "scheduled arrival: " + data.scheduledArrival.toString()
            tv_resource_details_scheduled_arrival.text = arrivalText

        }
    }
}
