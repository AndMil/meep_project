package com.meep.mobile.android.appname.app.common.widget.empty

interface EmptyListener {

    fun onCheckAgainClicked()
}