package com.meep.mobile.android.appname.app.di

import androidx.room.Room
import com.meep.mobile.android.appname.app.BuildConfig
import com.meep.mobile.android.appname.app.UiThread
import com.meep.mobile.android.appname.app.common.errorhandling.ErrorBundleBuilder
import com.meep.mobile.android.appname.app.location.LocationHelper
import com.meep.mobile.android.appname.app.main.navigationdrawer.MainActivityViewModel
import com.meep.mobile.android.appname.app.resource.viewmodel.ResourceErrorBundleBuilder
import com.meep.mobile.android.appname.app.resource.viewmodel.ResourceViewModel
import com.meep.mobile.android.appname.app.splash.SplashActivityViewModel
import com.meep.mobile.android.appname.app.splash.SplashErrorBundleBuilder
import com.meep.mobile.android.appname.data.resource.repository.ResourceDataRepository
import com.meep.mobile.android.appname.data.resource.source.ResourceCacheDataSource
import com.meep.mobile.android.appname.data.resource.source.ResourceDataStoreFactory
import com.meep.mobile.android.appname.data.resource.source.ResourceRemoteDataSource
import com.meep.mobile.android.appname.datasources.resource.cache.PreferencesHelper
import com.meep.mobile.android.appname.datasources.resource.cache.ResourceCacheImpl
import com.meep.mobile.android.appname.datasources.resource.cache.db.ResourceDatabase
import com.meep.mobile.android.appname.datasources.resource.cache.mapper.CachedResourceMapper
import com.meep.mobile.android.appname.datasources.resource.remote.ResourceRemoteImpl
import com.meep.mobile.android.appname.datasources.resource.remote.ResourceServiceFactory
import com.meep.mobile.android.appname.datasources.resource.remote.mapper.ResourceRemoteMapper
import com.meep.mobile.android.appname.domain.executor.JobExecutor
import com.meep.mobile.android.appname.domain.executor.PostExecutionThread
import com.meep.mobile.android.appname.domain.executor.ThreadExecutor
import com.meep.mobile.android.appname.domain.resource.interactor.GetResources
import com.meep.mobile.android.appname.domain.resource.repository.ResourceRepository
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * This file is where all Koin modules are defined.
 *
 * [The original template](https://github.com/bufferapp/clean-architecture-koin-boilerplate) was a
 * port of a [Dagger template](https://github.com/bufferapp/android-clean-architecture-boilerplate),
 * and the explanation of the dependency injection porting was explained in this
 * [post](https://overflow.buffer.com/2018/09/13/a-brief-look-at-koin-on-android/).
 */
val applicationModule = module(override = true) {

    single { PreferencesHelper(androidContext()) }

    factory { ResourceRemoteMapper() }

    single { JobExecutor() as ThreadExecutor }
    single { UiThread() as PostExecutionThread }

    single {
        Room.databaseBuilder(
            androidContext(),
            ResourceDatabase::class.java, "resources.db"
        )
            .build()
    }
    factory { get<ResourceDatabase>().cachedResourceDao() }
    factory { LocationHelper(androidContext()) }

    // IMPORTANT: Named qualifiers must be unique inside the module
    factory<ResourceRemoteDataSource> { ResourceRemoteImpl(get(), get()) }
    factory<ResourceCacheDataSource> { ResourceCacheImpl(get(), get(), get(), androidContext()) }
    factory { ResourceDataStoreFactory(get(), get()) }

    factory { CachedResourceMapper() }
    factory { ResourceServiceFactory.makeResourceService(androidContext(), BuildConfig.DEBUG) }

    factory<ResourceRepository> { ResourceDataRepository(get()) }
}

val splashModule = module(override = true) {
    factory<ErrorBundleBuilder>(named("splashErrorBundleBuilder")) { SplashErrorBundleBuilder() }
    viewModel { SplashActivityViewModel(get(named("splashErrorBundleBuilder"))) }
}

val mainModule = module(override = true) {
    viewModel { MainActivityViewModel() }
}

val resourceModule = module(override = true) {
    factory { GetResources(get(), get(), get()) }
    factory<ErrorBundleBuilder>(named("resourcesErrorBundleBuilder")) { ResourceErrorBundleBuilder() }
    viewModel { ResourceViewModel(get(), get(named("resourcesErrorBundleBuilder"))) }
}