package com.meep.mobile.android.appname.app.common.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory

object ResourceUtils {

    val DRAWABLE_TYPE = "drawable"
    val STRING_TYPE = "string"

    @JvmStatic
    fun getResourceId(context: Context, name: String, defType: String): Int {
        return context.resources.getIdentifier(name, defType, context.packageName)
    }

    @JvmStatic
    fun getColorString(context: Context, @ColorRes colorId: Int): String {
        val resourceColorId = ContextCompat.getColor(context, colorId)
        val colorString = String.format("%X", resourceColorId)

        return if (colorString.length <= 6) colorString else colorString.substring(2) /*!!strip alpha value!!*/
    }

    fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }
}
