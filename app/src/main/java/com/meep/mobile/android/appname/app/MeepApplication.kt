package com.meep.mobile.android.appname.app

import android.app.Application
import com.meep.mobile.android.appname.app.di.applicationModule
import com.meep.mobile.android.appname.app.di.mainModule
import com.meep.mobile.android.appname.app.di.resourceModule
import com.meep.mobile.android.appname.app.di.splashModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class MeepApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // start Koin!
        startKoin {
            // Android context
            androidContext(this@MeepApplication)
            // modules
            modules(listOf(applicationModule, splashModule, mainModule, resourceModule))
        }

        setupTimber()
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
