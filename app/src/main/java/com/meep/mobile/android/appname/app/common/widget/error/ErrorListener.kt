package com.meep.mobile.android.appname.app.common.widget.error

import com.meep.mobile.android.appname.app.common.errorhandling.ErrorBundle

interface ErrorListener {

    fun onRetry(errorBundle: ErrorBundle)
}