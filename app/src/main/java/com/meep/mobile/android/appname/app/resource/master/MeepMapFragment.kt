package com.meep.mobile.android.appname.app.resource.master

import android.Manifest
import android.content.Context
import android.content.res.Resources.NotFoundException
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.meep.mobile.android.appname.app.R
import com.meep.mobile.android.appname.app.common.BaseFragment
import com.meep.mobile.android.appname.app.common.errorhandling.AppAction
import com.meep.mobile.android.appname.app.common.errorhandling.AppAction.GET_RESOURCES
import com.meep.mobile.android.appname.app.common.errorhandling.AppError.NO_INTERNET
import com.meep.mobile.android.appname.app.common.errorhandling.AppError.TIMEOUT
import com.meep.mobile.android.appname.app.common.errorhandling.ErrorBundle
import com.meep.mobile.android.appname.app.common.errorhandling.ErrorDialogFragment
import com.meep.mobile.android.appname.app.common.errorhandling.ErrorDialogFragment.ErrorDialogFragmentListener
import com.meep.mobile.android.appname.app.common.errorhandling.ErrorUtils
import com.meep.mobile.android.appname.app.common.model.ResourceState.*
import com.meep.mobile.android.appname.app.common.util.ResourceUtils.bitmapDescriptorFromVector
import com.meep.mobile.android.appname.app.common.widget.empty.EmptyListener
import com.meep.mobile.android.appname.app.common.widget.error.ErrorListener
import com.meep.mobile.android.appname.app.location.LocationHelper
import com.meep.mobile.android.appname.app.main.navigationdrawer.MainActivity
import com.meep.mobile.android.appname.app.resource.model.ResourceItem
import com.meep.mobile.android.appname.app.resource.viewmodel.MeepResourceState
import com.meep.mobile.android.appname.app.resource.viewmodel.ResourceViewModel
import com.meep.mobile.android.appname.model.resource.Resource
import kotlinx.android.synthetic.main.fragment_resources.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber


class MeepMapFragment : BaseFragment(), ErrorDialogFragmentListener,
        LocationHelper.CurrentLocationListener, MainActivity.GeoLocChangeListener {

    private lateinit var map: GoogleMap
    private val resourceViewModel: ResourceViewModel by sharedViewModel()
    private val locationHelper: LocationHelper by inject()

    // Just for testing purposes
    private val lisboaLatLngLeft = LatLng(38.711046, -9.160096)
    private val lisboaLatLngRight = LatLng(38.739429, -9.137115)
    private var mapReady: Boolean = false
    private var setCluster: Boolean = false

    // region Lifecycle methods
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_resources, container, false)
    }

    override fun onResume() {
        super.onResume()

        activity?.setTitle(R.string.resource_master_title)
    }

    override fun earlyInitializeViews() {
        super.earlyInitializeViews()
        setupViewListeners()
        (childFragmentManager.findFragmentById(R.id.mapFragmentEmbedded) as SupportMapFragment).getMapAsync {
            try {
                it.setMapStyle(MapStyleOptions.loadRawResourceStyle(
                        context, R.raw.map_style))
            } catch (e: NotFoundException) {
                Timber.e(e)
            }
            map = it
            mapReady = true

            if (!setCluster) {
                resourceViewModel.fetchResources(lisboaLatLngLeft, lisboaLatLngRight)
            }
        }
    }

    override fun initializeContents(savedInstanceState: Bundle?) {
        super.initializeContents(savedInstanceState)

        resourceViewModel.getResources().observe(viewLifecycleOwner,
                Observer<MeepResourceState> {
                    if (it != null) this.handleDataState(it)
                }
        )

        if (isLocationActivated()) {
            getLocation()
        }
    }

    //endregion

    private fun setUpCluster(data: List<Resource>) {
        if (mapReady) {
            val clusterManager = ClusterManager<ResourceItem>(context, map)
            context?.let {
                clusterManager.renderer = ResourceCustomRenderer(it, map, clusterManager)
            }
            clusterManager.setOnClusterItemClickListener {
                resourceViewModel.select(it.getIdResource())
                findNavController().navigate(R.id.resourceDetailFragment)
                true
            }

            clusterManager.setOnClusterItemInfoWindowClickListener {
                resourceViewModel.select(it.getIdResource())
                findNavController().navigate(R.id.resourceDetailFragment)
            }
            map.setOnCameraIdleListener(clusterManager)
            map.setOnMarkerClickListener(clusterManager)
            clusterManager.clearItems()
            data.map {
                clusterManager.addItem(ResourceItem(it.id, it.y, it.x, it.name, it.companyZoneId))
            }

            map.moveCamera(CameraUpdateFactory.newLatLngZoom(lisboaLatLngLeft, 10f))
            setCluster = true
        }
    }

    //region Handling state
    private fun handleDataState(meepResourceState: MeepResourceState) {
        when (meepResourceState) {
            is Loading -> setupScreenForLoadingState()
            is Success -> setupScreenForSuccess(meepResourceState.data)
            is Error -> setupScreenForError(meepResourceState.errorBundle)
        }
    }

    private fun setupScreenForLoadingState() {
        lv_resources_loading_view.visibility = View.VISIBLE
        ev_resources_empty_view.visibility = View.GONE
        ev_resources_error_view.visibility = View.GONE
    }

    private fun setupScreenForSuccess(data: List<Resource>?) {
        ev_resources_error_view.visibility = View.GONE
        lv_resources_loading_view.visibility = View.GONE
        if (data != null && data.isNotEmpty()) {
            setUpCluster(data)
        } else {
            ev_resources_empty_view.visibility = View.VISIBLE
        }
    }

    private fun setupScreenForError(errorBundle: ErrorBundle) {
        lv_resources_loading_view.visibility = View.GONE
        //rv_resources.visibility = View.GONE
        ev_resources_empty_view.visibility = View.GONE

        if (errorBundle.appError == NO_INTERNET || errorBundle.appError == TIMEOUT) {
            // Example of using a custom error view as part of the fragment view
            showErrorView(errorBundle)
        } else {
            // Example of using an error fragment dialog
            showErrorDialog(errorBundle)
        }
    }
    //endregion

    //region Error and empty views
    private fun setupViewListeners() {
        ev_resources_empty_view.emptyListener = emptyListener
        ev_resources_error_view.errorListener = errorListener
    }

    private val emptyListener = object : EmptyListener {
        override fun onCheckAgainClicked() {
            resourceViewModel.fetchResources(lisboaLatLngLeft, lisboaLatLngRight)
        }
    }

    private val errorListener = object : ErrorListener {
        override fun onRetry(errorBundle: ErrorBundle) {
            retry(errorBundle.appAction)
        }
    }

    private fun retry(appAction: AppAction) {
        when (appAction) {
            GET_RESOURCES -> resourceViewModel.fetchResources(lisboaLatLngLeft, lisboaLatLngRight)
            else -> Timber.e("Unknown action code")
        }
    }

    private fun showErrorView(errorBundle: ErrorBundle) {
        activity?.let { activity ->
            ev_resources_error_view.visibility = View.VISIBLE
            ev_resources_error_view.errorBundle = errorBundle
            ev_resources_error_view.setErrorMessage(ErrorUtils.buildErrorMessageForDialog(activity, errorBundle).message)
        } ?: Timber.e("Activity is null")
    }

    private fun showErrorDialog(errorBundle: ErrorBundle) {
        val tag = ErrorDialogFragment.TAG
        activity?.let { activity ->
            val fragmentManager = activity.supportFragmentManager
            val previousDialogFragment = fragmentManager.findFragmentByTag(tag) as? DialogFragment

            // Check that error dialog is not already shown after a screen rotation
            val previousDialogFragmentDialog = previousDialogFragment?.dialog
            if (previousDialogFragment != null
                    && !previousDialogFragment.isRemoving
                    && previousDialogFragmentDialog != null
                    && previousDialogFragmentDialog.isShowing
            ) {
                // Error dialog is shown
                Timber.w("Error dialog is already shown")
            } else {
                // Error dialog is not shown
                val errorDialogFragment = ErrorDialogFragment.newInstance(
                        ErrorUtils.buildErrorMessageForDialog(activity, errorBundle),
                        true
                )
                if (!fragmentManager.isDestroyed && !fragmentManager.isStateSaved) {
                    errorDialogFragment.isCancelable = false
                    errorDialogFragment.show(fragmentManager, tag)
                }
            }
        } ?: Timber.e("Activity is null")
    }

    override fun onErrorDialogAccepted(action: Long, retry: Boolean) {
        if (retry) {
            retry(AppAction.fromCode(action))
        } else {
            Timber.d("There was no retry option for action \'$action\' given to the user.")
        }
    }

    override fun onErrorDialogCancelled(action: Long) {
        Timber.d("User cancelled error dialog")
    }
    //endregion

    private fun getLocation() {
        activity?.let {
            if (!locationHelper.getCurrentLocation(this)) { // Location permission not granted
                (it as MainActivity).setListenerWeakReference(this)
                ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), locationCode)
            }
        }
    }

    override fun onNewLocation(location: Location?) {
        location?.let {
            val latLng = LatLng(location.latitude, location.longitude)
            //  resourceViewModel.fetchResources(latLng, latLng)
            map.addMarker(MarkerOptions().position(latLng)
                    .title("Current position"))
                    .setIcon(bitmapDescriptorFromVector(context!!, R.drawable.ic_user))
        }
    }

    private fun isLocationActivated(): Boolean {
        val lm = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    override fun geoLocChanged() {
        getLocation()
    }
}
