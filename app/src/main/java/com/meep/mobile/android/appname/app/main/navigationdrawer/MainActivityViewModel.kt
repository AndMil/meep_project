package com.meep.mobile.android.appname.app.main.navigationdrawer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.meep.mobile.android.appname.app.common.model.ResourceState
import io.reactivex.disposables.Disposable
import kotlin.properties.Delegates

typealias NavigationDrawerSignOutState = ResourceState<Void?>

class MainActivityViewModel() : ViewModel() {
    var currentMenuItemId: Int by Delegates.notNull()

    private val navigationDrawerSignOutLiveData: MutableLiveData<NavigationDrawerSignOutState> = MutableLiveData()
    private var disposable: Disposable? = null

    override fun onCleared() {
        disposable?.dispose()

        super.onCleared()
    }

    fun getSignOut(): LiveData<NavigationDrawerSignOutState> {
        return navigationDrawerSignOutLiveData
    }

    fun signOut() {
        TODO()
    }
}
