package com.meep.mobile.android.appname.app.resource.master

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.meep.mobile.android.appname.app.R
import com.meep.mobile.android.appname.app.common.util.ResourceUtils.bitmapDescriptorFromVector
import com.meep.mobile.android.appname.app.resource.model.ResourceItem


class ResourceCustomRenderer(val context: Context, map: GoogleMap, clusterManager: ClusterManager<ResourceItem>)
    : DefaultClusterRenderer<ResourceItem>(context, map, clusterManager) {

    override fun onBeforeClusterItemRendered(resourceItem: ResourceItem, markerOptions: MarkerOptions) {
        markerOptions.icon(bitmapDescriptorFromVector(context, getResource(resourceItem.getCompanyZoneId())))
    }

    private fun getResource(companyZoneId: Int): Int {
        return when (companyZoneId) {
            metroStationZoneId -> R.drawable.ic_directions_metro
            busStationZoneId -> R.drawable.ic_directions_bus
            mopedZoneId -> R.drawable.ic_directions_car
            bikeZoneId -> R.drawable.ic_directions_bike
            else -> R.drawable.ic_place
        }
    }
}
