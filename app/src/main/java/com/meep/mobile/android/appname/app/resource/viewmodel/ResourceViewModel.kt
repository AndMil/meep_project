package com.meep.mobile.android.appname.app.resource.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.meep.mobile.android.appname.app.common.errorhandling.AppAction.GET_RESOURCES
import com.meep.mobile.android.appname.app.common.errorhandling.ErrorBundleBuilder
import com.meep.mobile.android.appname.app.common.model.ResourceState
import com.meep.mobile.android.appname.app.common.model.ResourceState.*
import com.meep.mobile.android.appname.domain.resource.interactor.GetResources
import com.meep.mobile.android.appname.model.resource.LatLngModel
import com.meep.mobile.android.appname.model.resource.Resource
import io.reactivex.disposables.Disposable
import timber.log.Timber

typealias MeepResourceState = ResourceState<List<Resource>>

class ResourceViewModel(private val getResourcesUseCase: GetResources, private val errorBundleBuilder: ErrorBundleBuilder) : ViewModel() {

    private val meepResourceLiveData: MutableLiveData<MeepResourceState> = MutableLiveData()
    private val relectedResourceLiveData: MutableLiveData<Resource> = MutableLiveData()
    private var resources: List<Resource> = emptyList()
    private var disposable: Disposable? = null

    override fun onCleared() {
        disposable?.dispose()

        super.onCleared()
    }

    fun getResources(): LiveData<MeepResourceState> {
        return meepResourceLiveData
    }

    fun getSelectedResource(): LiveData<Resource> {
        return relectedResourceLiveData
    }

    fun select(id: String) {
        relectedResourceLiveData.value = resources.firstOrNull { it.id == id }
    }

    fun fetchResources(lowerLeftLatLong: LatLng, upperRightLatLng: LatLng) {
        meepResourceLiveData.value = Loading()
        disposable = getResourcesUseCase.execute(Pair(LatLngModel(lowerLeftLatLong.latitude, lowerLeftLatLong.longitude),
                LatLngModel(upperRightLatLng.latitude, upperRightLatLng.longitude)))
                .subscribe({
                    this@ResourceViewModel.resources = it
                    meepResourceLiveData.value = Success(this@ResourceViewModel.resources)
                },{
                    Timber.d(it)
                    meepResourceLiveData.value = Error(errorBundleBuilder.build(it, GET_RESOURCES))
                })
    }
}