package com.meep.mobile.android.appname.app.resource.master

sealed class ResourceNavigationCommand {

    object GoToDetailsView : ResourceNavigationCommand()
}