package com.meep.mobile.android.appname.app.location

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

class LocationHelper(val context: Context) {

    private var fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    private lateinit var currentLocationListener: CurrentLocationListener
    private lateinit var locationRequest: LocationRequest

    interface CurrentLocationListener {
        fun onNewLocation(location: Location?)
    }

    /**
     * Returns false if location permission is not granted
     */
    fun getCurrentLocation(currentLocationListener: CurrentLocationListener): Boolean {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            this.currentLocationListener = currentLocationListener
            createLocationRequest()

            return true
        }

        return false
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.numUpdates = 1

        fusedLocationClient.requestLocationUpdates(locationRequest, object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)

                fusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->
                    currentLocationListener.onNewLocation(location)
                }
            }
        }, Looper.getMainLooper())
    }

}