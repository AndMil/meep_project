package com.meep.mobile.android.appname.app.splash

import android.os.Bundle
import com.meep.mobile.android.appname.app.common.BaseActivity
import com.meep.mobile.android.appname.app.common.errorhandling.ErrorDialogFragment.ErrorDialogFragmentListener
import com.meep.mobile.android.appname.app.common.navigation.Navigator
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class SplashActivity : BaseActivity(), ErrorDialogFragmentListener {

    private val splashActivityViewModel: SplashActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // TODO Check credentials
        Navigator.navigateToMainActivity(this)
    }

    override fun onErrorDialogAccepted(action: Long, retry: Boolean) {
        // TODO: Implement
        Timber.d("User accepted error dialog")
    }

    override fun onErrorDialogCancelled(action: Long) {
        // TODO: Implement
        Timber.d("User cancelled error dialog")
    }
}