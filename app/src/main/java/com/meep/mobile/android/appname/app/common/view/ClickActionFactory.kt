package com.meep.mobile.android.appname.app.common.view

import android.view.View

interface ClickActionFactory {

    fun onClickAction(widget: View)
}