package com.meep.mobile.android.appname.app.resource.model

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class ResourceItem(idResource: String, lat: Double, lng: Double, title: String, companyZoneId: Int) : ClusterItem {

    private var idResource = ""
    private var position: LatLng? = null
    private var title: String? = null
    private val snippet: String? = null
    private var companyZoneId: Int = 0

    init {
        position = LatLng(lat, lng)
        this.idResource = idResource
        this.title = title
        this.companyZoneId = companyZoneId
    }

    override fun getSnippet(): String? {
        return snippet
    }

    override fun getTitle(): String? {
        return title
    }

    override fun getPosition(): LatLng? {
        return position
    }

    fun getCompanyZoneId(): Int {
        return companyZoneId
    }

    fun getIdResource(): String {
        return idResource
    }
}