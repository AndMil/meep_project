package com.meep.mobile.android.appname.app.splash

import androidx.lifecycle.ViewModel
import com.meep.mobile.android.appname.app.common.errorhandling.ErrorBundleBuilder
import io.reactivex.disposables.Disposable

class SplashActivityViewModel(private val errorBundleBuilder: ErrorBundleBuilder) : ViewModel() {
    private var disposable: Disposable? = null

    override fun onCleared() {
        disposable?.dispose()

        super.onCleared()
    }
}
