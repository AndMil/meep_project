package com.meep.mobile.android.appname.app.common.viewmodel

sealed class CommonEvent {

    object Unauthorized : CommonEvent()
}