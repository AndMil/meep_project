package com.meep.mobile.android.appname.app.test.util

import com.meep.mobile.android.appname.model.resource.Resource

/**
 * Factory class for resource related instances
 */
object ResourceFactory {

    fun makeResourceList(count: Int): List<Resource> {
        val resources = mutableListOf<Resource>()
        repeat(count) {
            resources.add(makeresourceModel())
        }
        return resources
    }

    fun makeresourceModel(): Resource {
        return Resource(
                DataFactory.randomUuid(),
                DataFactory.randomUuid(),
                DataFactory.randomDouble(),
                DataFactory.randomDouble(),
                DataFactory.randomDouble(),
                DataFactory.randomInt(),
                DataFactory.randomInt(),
                DataFactory.randomDouble(),
                DataFactory.randomDouble()
        )
    }
}