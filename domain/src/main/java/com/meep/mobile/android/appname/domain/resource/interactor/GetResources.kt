package com.meep.mobile.android.appname.domain.resource.interactor

import com.meep.mobile.android.appname.domain.executor.PostExecutionThread
import com.meep.mobile.android.appname.domain.executor.ThreadExecutor
import com.meep.mobile.android.appname.domain.interactor.SingleUseCase
import com.meep.mobile.android.appname.domain.resource.repository.ResourceRepository
import com.meep.mobile.android.appname.model.resource.LatLngModel
import com.meep.mobile.android.appname.model.resource.Resource
import io.reactivex.Single

open class GetResources(
        private val resourceRepository: ResourceRepository,
        threadExecutor: ThreadExecutor,
        postExecutionThread: PostExecutionThread
) : SingleUseCase<List<Resource>, Pair<LatLngModel, LatLngModel>?>(threadExecutor, postExecutionThread) {

    public override fun buildUseCaseObservable(params: Pair<LatLngModel, LatLngModel>?): Single<List<Resource>> {
        checkNotNull(params)
        return resourceRepository.getResources(params.first, params.second)
    }
}