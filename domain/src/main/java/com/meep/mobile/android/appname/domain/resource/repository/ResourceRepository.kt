package com.meep.mobile.android.appname.domain.resource.repository

import com.meep.mobile.android.appname.model.resource.LatLngModel
import com.meep.mobile.android.appname.model.resource.Resource
import io.reactivex.Single

interface ResourceRepository {

    fun getResources(lowerLeftLatLongModel: LatLngModel, upperRightLatLngModel: LatLngModel): Single<List<Resource>>

}