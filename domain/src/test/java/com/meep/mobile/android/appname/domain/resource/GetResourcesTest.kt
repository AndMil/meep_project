package com.meep.mobile.android.appname.domain.resource

import com.meep.mobile.android.appname.domain.executor.PostExecutionThread
import com.meep.mobile.android.appname.domain.executor.ThreadExecutor
import com.meep.mobile.android.appname.domain.resource.interactor.GetResources
import com.meep.mobile.android.appname.domain.resource.repository.ResourceRepository
import com.meep.mobile.android.appname.domain.test.factory.ResourceFactory
import com.meep.mobile.android.appname.domain.test.factory.ResourceFactory.makeLatLngModel
import com.meep.mobile.android.appname.model.resource.Resource
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Test

class GetResourcesTest {

    private val mockThreadExecutor = mock<ThreadExecutor>()
    private val mockPostExecutionThread = mock<PostExecutionThread>()
    private val mockResourceRepository = mock<ResourceRepository>()

    private val getResources = GetResources(
        mockResourceRepository, mockThreadExecutor,
        mockPostExecutionThread
    )
    private val mockLatLongLeft = makeLatLngModel()
    private val mockLatLongRight = makeLatLngModel()

    @Test
    fun buildUseCaseObservableCallsRepository() {
        getResources.buildUseCaseObservable(Pair(mockLatLongLeft, mockLatLongRight))
        verify(mockResourceRepository).getResources(mockLatLongLeft, mockLatLongRight)
    }

    @Test
    fun buildUseCaseObservableCompletes() {
        stubResourceRepositoryGetResources(Single.just(ResourceFactory.makeResourceList(2)))
        val testObserver = getResources.buildUseCaseObservable(Pair(mockLatLongLeft, mockLatLongRight)).test()
        testObserver.assertComplete()
    }

    @Test
    fun buildUseCaseObservableReturnsData() {
        val resources = ResourceFactory.makeResourceList(2)
        stubResourceRepositoryGetResources(Single.just(resources))
        val testObserver = getResources.buildUseCaseObservable(Pair(mockLatLongLeft, mockLatLongRight)).test()
        testObserver.assertValue(resources)
    }

    private fun stubResourceRepositoryGetResources(single: Single<List<Resource>>) {
        whenever(mockResourceRepository.getResources(mockLatLongLeft, mockLatLongRight))
            .thenReturn(single)
    }
}