package com.meep.mobile.android.appname.domain.test.factory

import com.meep.mobile.android.appname.domain.test.factory.DataFactory.Factory.randomDouble
import com.meep.mobile.android.appname.domain.test.factory.DataFactory.Factory.randomInt
import com.meep.mobile.android.appname.domain.test.factory.DataFactory.Factory.randomUuid
import com.meep.mobile.android.appname.model.resource.LatLngModel
import com.meep.mobile.android.appname.model.resource.Resource

/**
 * Factory class for Bufferoo related instances
 */
object ResourceFactory {


    fun makeLatLngModel(): LatLngModel {
        return LatLngModel(
                randomDouble(),
                randomDouble()
        )
    }

    fun makeResource(): Resource {
        return Resource(
                randomUuid(),
                randomUuid(),
                randomDouble(),
                randomDouble(),
                randomDouble(),
                randomInt(),
                randomInt(),
                randomDouble(),
                randomDouble())
    }

    fun makeResourceList(count: Int): List<Resource> {
        val bufferoos = mutableListOf<Resource>()
        repeat(count) {
            bufferoos.add(makeResource())
        }
        return bufferoos
    }
}