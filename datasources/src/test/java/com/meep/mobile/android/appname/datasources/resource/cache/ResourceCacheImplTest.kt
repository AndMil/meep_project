package com.meep.mobile.android.appname.datasources.resource.cache

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.meep.mobile.android.appname.datasources.resource.cache.db.ResourceDatabase
import com.meep.mobile.android.appname.datasources.resource.cache.mapper.CachedResourceMapper
import com.meep.mobile.android.appname.datasources.resource.cache.model.CachedResource
import com.meep.mobile.android.appname.datasources.resource.cache.test.factory.ResourceFactory
import com.meep.mobile.android.appname.datasources.resource.cache.test.factory.ResourceFactory.Factory.makeLatLngModel
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertEquals

@RunWith(AndroidJUnit4::class)
class ResourceCacheImplTest {

    private var resourcesDatabase = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            ResourceDatabase::class.java
    ).allowMainThreadQueries().build()
    private var cachedResourceMapper = CachedResourceMapper()
    private var preferencesHelper = PreferencesHelper(ApplicationProvider.getApplicationContext())

    private val mockLatLong = makeLatLngModel()

    private val databaseHelper = ResourceCacheImpl(
            resourcesDatabase,
            cachedResourceMapper,
            preferencesHelper,
            ApplicationProvider.getApplicationContext()
    )

    @Test
    fun clearTablesCompletes() {
        val testObserver = databaseHelper.clearResources().test()
        testObserver.assertComplete()
    }

    //<editor-fold desc="Save Resources">
    @Test
    fun saveResourcesCompletes() {
        val resourceEntities = ResourceFactory.makeResourceEntityList(2)

        val testObserver = databaseHelper.saveResources(resourceEntities).test()
        testObserver.assertComplete()
    }

    @Test
    fun saveResourcesSavesData() {
        val resourceCount = 2
        val resourceEntities = ResourceFactory.makeResourceEntityList(resourceCount)

        databaseHelper.saveResources(resourceEntities).test()
        checkNumRowsInResourcesTable(resourceCount)
    }
    //</editor-fold>

    //<editor-fold desc="Get Resources">
    @Test
    fun getResourcesCompletes() {
        val testObserver = databaseHelper.getResources(mockLatLong, mockLatLong).test()
        testObserver.assertComplete()
    }

    @Test
    fun getResourcesReturnsData() {
        val resourceEntities = ResourceFactory.makeResourceEntityList(2)
        val cachedResources = mutableListOf<CachedResource>()
        resourceEntities.forEach {
            cachedResources.add(cachedResourceMapper.mapToCached(it))
        }
        insertResources(cachedResources)

        val testObserver = databaseHelper.getResources(mockLatLong, mockLatLong).test()
        testObserver.assertValue(resourceEntities)
    }
    //</editor-fold>

    private fun insertResources(cachedResources: List<CachedResource>) {
        cachedResources.forEach {
            resourcesDatabase.cachedResourceDao().insertResource(it)
        }
    }

    private fun checkNumRowsInResourcesTable(expectedRows: Int) {
        val numberOfRows = resourcesDatabase.cachedResourceDao().getResources().size
        assertEquals(expectedRows, numberOfRows)
    }
}