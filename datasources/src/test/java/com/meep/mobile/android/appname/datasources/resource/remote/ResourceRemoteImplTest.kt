package com.meep.mobile.android.appname.datasources.resource.remote

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.meep.mobile.android.appname.datasources.resource.cache.test.factory.ResourceFactory
import com.meep.mobile.android.appname.datasources.resource.remote.mapper.ResourceRemoteMapper
import com.meep.mobile.android.appname.datasources.resource.remote.model.ResourceRemoteModel
import com.meep.mobile.android.appname.datasources.resource.remote.test.factory.ResourceFactory.Factory.makeResourceRemoteModelList
import com.meep.mobile.android.appname.model.resource.Resource
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ResourceRemoteImplTest {

    private val entityMapper = mock<ResourceRemoteMapper>()
    private val resourceService = mock<ResourceService>()

    private val resourceRemoteImpl = ResourceRemoteImpl(resourceService, entityMapper)
    private val mockLatLong = ResourceFactory.makeLatLngModel()


    //<editor-fold desc="Get Resources">
    @Test
    fun getResourcesCompletes() {
        stubResourceServiceGetResources(Single.just(makeResourceRemoteModelList(2)))
        val testObserver = resourceRemoteImpl.getResources(mockLatLong, mockLatLong).test()
        testObserver.assertComplete()
    }

    @Test
    fun getResourcesReturnsData() {
        val resourceResponse = makeResourceRemoteModelList(2)
        stubResourceServiceGetResources(Single.just(resourceResponse))
        val resourceEntities = mutableListOf<Resource>()
        resourceResponse.map {
            resourceEntities.add(entityMapper.mapFromRemote(it))
        }

        val testObserver = resourceRemoteImpl.getResources(mockLatLong, mockLatLong).test()
        testObserver.assertValue(resourceEntities)
    }
    //</editor-fold>

    private fun stubResourceServiceGetResources(observable: Single<List<ResourceRemoteModel>>) {
        whenever(resourceService.getResources(mockLatLong, mockLatLong))
            .thenReturn(observable)
    }
}