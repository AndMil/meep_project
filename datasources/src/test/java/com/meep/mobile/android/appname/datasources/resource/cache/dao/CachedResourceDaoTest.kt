package com.meep.mobile.android.appname.datasources.resource.cache.dao

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.meep.mobile.android.appname.datasources.resource.cache.db.ResourceDatabase
import com.meep.mobile.android.appname.datasources.resource.cache.test.factory.ResourceFactory
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
open class CachedResourceDaoTest {

    private lateinit var mResourceDatabase: ResourceDatabase

    @Before
    fun initDb() {
        mResourceDatabase = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            ResourceDatabase::class.java
        )
            .allowMainThreadQueries()
            .build()
    }

    @After
    fun closeDb() {
        mResourceDatabase.close()
    }

    @Test
    fun insertResourcesSavesData() {
        val cachedResource = ResourceFactory.makeCachedResource()
        mResourceDatabase.cachedResourceDao().insertResource(cachedResource)

        val resources = mResourceDatabase.cachedResourceDao().getResources()
        assert(resources.isNotEmpty())
    }

    @Test
    fun getResourcesRetrievesData() {
        val cachedResources = ResourceFactory.makeCachedResourceList(5)

        cachedResources.forEach {
            mResourceDatabase.cachedResourceDao().insertResource(it)
        }

        val retrievedResources = mResourceDatabase.cachedResourceDao().getResources()

        assert(cachedResources.containsAll(retrievedResources))
    }
}