package com.meep.mobile.android.appname.datasources.resource.remote.mapper

import com.meep.mobile.android.appname.datasources.resource.remote.test.factory.ResourceFactory
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class ResourceRemoteMapperTest {

    private val resourceEntityMapper = ResourceRemoteMapper()

    @Test
    fun mapFromRemoteMapsData() {
        val resourceResponse = ResourceFactory.makeResourceRemoteModel()
        val resourceEntity = resourceEntityMapper.mapFromRemote(resourceResponse)

        assertEquals(resourceResponse.id, resourceEntity.id)
        assertEquals(resourceResponse.name, resourceEntity.name)
        assertEquals(resourceResponse.x, resourceEntity.x)
        assertEquals(resourceResponse.y, resourceEntity.y)
        assertEquals(resourceResponse.lat, resourceEntity.lat)
        assertEquals(resourceResponse.lon, resourceEntity.lon)
        assertEquals(resourceResponse.locationType, resourceEntity.locationType)
        assertEquals(resourceResponse.companyZoneId, resourceEntity.companyZoneId)
    }
}