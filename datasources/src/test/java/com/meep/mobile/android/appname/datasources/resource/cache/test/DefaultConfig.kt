package com.meep.mobile.android.appname.datasources.resource.cache.test

object DefaultConfig {
    //The api level that RoboElectric will use to run the unit tests
    const val EMULATE_SDK = 21
}