package com.meep.mobile.android.appname.datasources.resource.remote.test.factory

import com.meep.mobile.android.appname.datasources.resource.remote.model.ResourceRemoteModel
import com.meep.mobile.android.appname.datasources.resource.remote.test.factory.DataFactory.Factory.randomDouble
import com.meep.mobile.android.appname.datasources.resource.remote.test.factory.DataFactory.Factory.randomInt
import com.meep.mobile.android.appname.datasources.resource.remote.test.factory.DataFactory.Factory.randomUuid
import com.meep.mobile.android.appname.model.resource.LatLngModel

/**
 * Factory class for Resource related instances
 */
class ResourceFactory {

    companion object Factory {

        fun makeLatLngModel(): LatLngModel {
            return LatLngModel(
                    randomDouble(),
                    randomDouble()
            )
        }

        fun makeResourceRemoteModelList(count: Int): List<ResourceRemoteModel> {
            val resourceEntities = mutableListOf<ResourceRemoteModel>()
            repeat(count) {
                resourceEntities.add(makeResourceRemoteModel())
            }
            return resourceEntities
        }

        fun makeResourceRemoteModel(): ResourceRemoteModel {
            return ResourceRemoteModel(
                    randomUuid(),
                    randomUuid(),
                    randomDouble(),
                    randomDouble(),
                    randomDouble(),
                    randomInt(),
                    randomInt(),
                    randomDouble(),
                    randomDouble(),
                    randomUuid(),
                    randomInt(),
                    randomInt(),
                    randomUuid()
            )
        }
    }
}