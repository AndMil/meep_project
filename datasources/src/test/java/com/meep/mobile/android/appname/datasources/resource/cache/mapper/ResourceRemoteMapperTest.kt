package com.meep.mobile.android.appname.datasources.resource.cache.mapper

import com.meep.mobile.android.appname.datasources.resource.cache.model.CachedResource
import com.meep.mobile.android.appname.datasources.resource.cache.test.factory.ResourceFactory
import com.meep.mobile.android.appname.model.resource.Resource
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class ResourceRemoteMapperTest {

    private val cachedResourceMapper = CachedResourceMapper()

    @Test
    fun mapToCachedMapsData() {
        val resourceEntity = ResourceFactory.makeResourceEntity()
        val cachedResource = cachedResourceMapper.mapToCached(resourceEntity)

        assertResourceDataEquality(resourceEntity, cachedResource)
    }

    @Test
    fun mapFromCachedMapsData() {
        val cachedResource = ResourceFactory.makeCachedResource()
        val resourceEntity = cachedResourceMapper.mapFromCached(cachedResource)

        assertResourceDataEquality(resourceEntity, cachedResource)
    }

    private fun assertResourceDataEquality(
            resource: Resource,
            cachedResource: CachedResource
    ) {
        assertEquals(resource.id, cachedResource.id)
        assertEquals(resource.name, cachedResource.name)
        assertEquals(resource.companyZoneId, cachedResource.companyZoneId)
        assertEquals(resource.x, cachedResource.x)
        assertEquals(resource.y, cachedResource.y)
        assertEquals(resource.lat, cachedResource.lat)
        assertEquals(resource.lon, cachedResource.lon)
    }
}