package com.meep.mobile.android.appname.datasources.resource.cache.test.factory

import com.meep.mobile.android.appname.datasources.resource.cache.model.CachedResource
import com.meep.mobile.android.appname.datasources.resource.cache.test.factory.DataFactory.Factory.randomDouble
import com.meep.mobile.android.appname.datasources.resource.cache.test.factory.DataFactory.Factory.randomInt
import com.meep.mobile.android.appname.datasources.resource.cache.test.factory.DataFactory.Factory.randomUuid
import com.meep.mobile.android.appname.model.resource.LatLngModel
import com.meep.mobile.android.appname.model.resource.Resource

/**
 * Factory class for Resource related instances
 */
class ResourceFactory {

    companion object Factory {

        fun makeLatLngModel(): LatLngModel {
            return LatLngModel(
                    randomDouble(),
                    randomDouble()
            )
        }

        fun makeCachedResource(): CachedResource {
            return CachedResource(
                    randomUuid(),
                    randomUuid(),
                    randomDouble(),
                    randomDouble(),
                    randomDouble(),
                    randomInt(),
                    randomInt(),
                    randomDouble(),
                    randomDouble()
            )
        }

        fun makeResourceEntity(): Resource {
            return Resource(
                    randomUuid(),
                    randomUuid(),
                    randomDouble(),
                    randomDouble(),
                    randomDouble(),
                    randomInt(),
                    randomInt(),
                    randomDouble(),
                    randomDouble())
        }

        fun makeResourceEntityList(count: Int): List<Resource> {
            val resourceEntities = mutableListOf<Resource>()
            repeat(count) {
                resourceEntities.add(makeResourceEntity())
            }
            return resourceEntities
        }

        fun makeCachedResourceList(count: Int): List<CachedResource> {
            val cachedResources = mutableListOf<CachedResource>()
            repeat(count) {
                cachedResources.add(makeCachedResource())
            }
            return cachedResources
        }
    }
}