package com.meep.mobile.android.appname.datasources.resource.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.meep.mobile.android.appname.datasources.resource.cache.db.constants.ResourcesConstants

/**
 * Model used solely for the caching of a bufferroo
 */
@Entity(tableName = ResourcesConstants.TABLE_NAME)
data class CachedResource(
    @PrimaryKey
    val id: String,
    val name: String,
    val x: Double,
    val y: Double,
    val scheduledArrival: Double,
    val locationType: Int,
    val companyZoneId: Int,
    val lat: Double,
    val lon: Double
)
