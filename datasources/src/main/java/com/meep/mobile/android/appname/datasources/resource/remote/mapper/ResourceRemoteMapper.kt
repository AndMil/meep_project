package com.meep.mobile.android.appname.datasources.resource.remote.mapper

import com.meep.mobile.android.appname.datasources.remote.mapper.EntityMapper
import com.meep.mobile.android.appname.datasources.resource.remote.model.ResourceRemoteModel
import com.meep.mobile.android.appname.model.resource.Resource

/**
 * Map a [] to and from a [Resource] instance when data is moving between
 * this later and the Data layer
 */
open class ResourceRemoteMapper : EntityMapper<ResourceRemoteModel, Resource> {

    /**
     * Map an instance of a [ResourceRemoteModel] to a [Resource] model
     */
    override fun mapFromRemote(type: ResourceRemoteModel): Resource {
        return Resource(
                type.id,
                type.name,
                type.x,
                type.y,
                type.scheduledArrival,
                type.locationType,
                type.companyZoneId,
                type.lat,
                type.lon)
    }
}