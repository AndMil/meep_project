package com.meep.mobile.android.appname.datasources.resource.remote

import com.meep.mobile.android.appname.datasources.resource.remote.model.PostCredentialsResponse
import com.meep.mobile.android.appname.datasources.resource.remote.model.PostCredentialsWithRefreshTokenRequest
import com.meep.mobile.android.appname.datasources.resource.remote.model.PostCredentialsWithUsernameRequest
import com.meep.mobile.android.appname.datasources.resource.remote.model.ResourceRemoteModel
import com.meep.mobile.android.appname.model.resource.LatLngModel
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Defines the abstract methods used for interacting with the Resource API
 */
interface ResourceService {

    companion object {
        const val BASE_URL = "https://apidev.meep.me/tripplan/api/"
        const val version = "v1"
        const val city = "lisboa"
    }

    @POST("oauth/token")
    fun postCredentials(@Body postCredentialsWithUsernameRequest: PostCredentialsWithUsernameRequest): Single<PostCredentialsResponse>

    @POST("oauth/token")
    fun postCredentials(@Body postCredentialsWithRefreshTokenRequest: PostCredentialsWithRefreshTokenRequest): Single<PostCredentialsResponse>

    @GET("$version/routers/$city/resources")
    fun getResources(@Query("lowerLeftLatLon") lowerLeftLatLonModel: LatLngModel, @Query("upperRightLatLon") upperRightLatLonModel: LatLngModel): Single<List<ResourceRemoteModel>>
}