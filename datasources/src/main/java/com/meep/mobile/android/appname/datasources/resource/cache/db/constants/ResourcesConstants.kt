package com.meep.mobile.android.appname.datasources.resource.cache.db.constants

/**
 * Defines constants for the Resources Table
 */
object ResourcesConstants {

    const val TABLE_NAME = "resources"
    const val QUERY_RESOURCES = "SELECT * FROM $TABLE_NAME"
    const val DELETE_ALL_RESOURCES = "DELETE FROM $TABLE_NAME"
}