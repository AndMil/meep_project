package com.meep.mobile.android.appname.datasources.resource.cache

import android.content.Context
import com.meep.mobile.android.appname.data.resource.source.ResourceCacheDataSource
import com.meep.mobile.android.appname.datasources.resource.cache.db.ResourceDatabase
import com.meep.mobile.android.appname.datasources.resource.cache.mapper.CachedResourceMapper
import com.meep.mobile.android.appname.datasources.resource.cache.model.CachedResource
import com.meep.mobile.android.appname.model.resource.LatLngModel
import com.meep.mobile.android.appname.model.resource.Resource
import io.reactivex.Completable
import io.reactivex.Single
import java.lang.ref.WeakReference

/**
 * Cached implementation for retrieving and saving resource instances. This class implements the
 * [ResourceCache] from the Data layer as it is that layers responsibility for defining the
 * operations in which data store implementation layers can carry out.
 */
class ResourceCacheImpl constructor(
        val resourcesDatabase: ResourceDatabase,
        private val cachedResourceMapper: CachedResourceMapper,
        private val preferencesHelper: PreferencesHelper,
        _context: Context
) : ResourceCacheDataSource {

    companion object {
        private const val EXPIRATION_TIME = (60 * 10 * 1000).toLong()
    }

    private val contextWeakReference = WeakReference<Context>(_context)

    // endregion

    // region resource
    /**
     * Retrieve an instance from the database, used for tests.
     */
    internal fun getDatabase(): ResourceDatabase {
        return resourcesDatabase
    }

    /**
     * Retrieve a list of [Resource] instances from the database.
     */

    override fun getResources(lowerLeftLatLongModel: LatLngModel, upperRightLatLngModel: LatLngModel): Single<List<Resource>> {
        return Single.defer {
            Single.just(resourcesDatabase.cachedResourceDao().getResources())
        }.map {
            it.map { cachedResourceMapper.mapFromCached(it) }
        }
    }

    /**
     * Save the given list of [Resource] instances to the database.
     */
    override fun saveResources(resources: List<Resource>): Completable {
        return Completable.defer {
            resources.forEach {
                resourcesDatabase.cachedResourceDao().insertResource(
                        cachedResourceMapper.mapToCached(it)
                )
            }
            setLastCacheTime(System.currentTimeMillis())
            Completable.complete()
        }
    }

    /**
     * Remove all the data from all the tables in the database.
     */
    override fun clearResources(): Completable {
        return Completable.defer {
            resourcesDatabase.cachedResourceDao().clearResources()
            Completable.complete()
        }
    }

    /**
     * Check whether there are instances of [CachedResource] stored in the cache.
     */
    override fun isValidCache(): Single<Boolean> {
        return Single.defer {
            val currentTime = System.currentTimeMillis()
            val lastUpdateTime = getLastCacheUpdateTimeMillis()
            val expired = currentTime - lastUpdateTime > EXPIRATION_TIME
            Single.just(resourcesDatabase.cachedResourceDao().getResources().isNotEmpty() && !expired)
        }
    }

    /**
     * Set a point in time at when the cache was last updated.
     */
    override fun setLastCacheTime(lastCache: Long) {
        preferencesHelper.lastCacheTime = lastCache
    }

    /**
     * Get in millis, the last time the cache was accessed.
     */
    private fun getLastCacheUpdateTimeMillis(): Long {
        return preferencesHelper.lastCacheTime
    }
    // endregion
}