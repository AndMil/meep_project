package com.meep.mobile.android.appname.datasources.resource.cache.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.meep.mobile.android.appname.datasources.resource.cache.dao.CachedResourceDao
import com.meep.mobile.android.appname.datasources.resource.cache.model.CachedResource

@Database(entities = [CachedResource::class], version = 1)
abstract class ResourceDatabase : RoomDatabase() {

    abstract fun cachedResourceDao(): CachedResourceDao
}