package com.meep.mobile.android.appname.datasources.resource.cache.mapper

import com.meep.mobile.android.appname.datasources.cache.mapper.EntityMapper
import com.meep.mobile.android.appname.datasources.resource.cache.model.CachedResource
import com.meep.mobile.android.appname.model.resource.Resource

/**
 * Map a [CachedResource] instance to and from a [Resource] instance when data is moving between
 * this later and the Data layer
 */
open class CachedResourceMapper : EntityMapper<CachedResource, Resource> {

    /**
     * Map a [Resource] instance to a [CachedResource] instance
     */
    override fun mapToCached(type: Resource): CachedResource {
        return CachedResource(type.id,
                type.name,
                type.x,
                type.y,
                type.scheduledArrival,
                type.locationType,
                type.companyZoneId,
                type.lat,
                type.lon)
    }

    /**
     * Map a [CachedResource] instance to a [Resource] instance
     */
    override fun mapFromCached(type: CachedResource): Resource {
        return Resource(
                type.id,
                type.name,
                type.x,
                type.y,
                type.scheduledArrival,
                type.locationType,
                type.companyZoneId,
                type.lat,
                type.lon)
    }
}