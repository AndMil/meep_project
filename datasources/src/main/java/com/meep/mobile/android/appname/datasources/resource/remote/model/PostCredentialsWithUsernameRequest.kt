package com.meep.mobile.android.appname.datasources.resource.remote.model

import com.google.gson.annotations.SerializedName

data class PostCredentialsWithUsernameRequest(
    @SerializedName("username")
    val username: String,
    @SerializedName("password")
    val password: String
)