package com.meep.mobile.android.appname.datasources.resource.remote.model

import com.google.gson.annotations.SerializedName

data class ResourceRemoteModel(
        @SerializedName("id")
        val id: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("x")
        val x: Double,
        @SerializedName("y")
        val y: Double,
        @SerializedName("scheduledArrival")
        val scheduledArrival: Double,
        @SerializedName("locationType")
        val locationType: Int,
        @SerializedName("companyZoneId")
        val companyZoneId: Int,
        @SerializedName("lat")
        val lat: Double,
        @SerializedName("lon")
        val lon: Double,
        @SerializedName("licencePlate")
        val licencePlate: String,
        @SerializedName("batteryLevel")
        val batteryLevel: Int,
        @SerializedName("helmets")
        val helmets: Int,
        @SerializedName ("model")
        val model: String)