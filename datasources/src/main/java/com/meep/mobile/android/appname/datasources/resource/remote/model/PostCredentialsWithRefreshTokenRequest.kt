package com.meep.mobile.android.appname.datasources.resource.remote.model

import com.google.gson.annotations.SerializedName

data class PostCredentialsWithRefreshTokenRequest (
    @SerializedName("refresh_token")
    val refresh_token: String
)