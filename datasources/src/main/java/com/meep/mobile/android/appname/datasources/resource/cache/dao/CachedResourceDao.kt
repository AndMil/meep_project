package com.meep.mobile.android.appname.datasources.resource.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.meep.mobile.android.appname.datasources.resource.cache.db.constants.ResourcesConstants
import com.meep.mobile.android.appname.datasources.resource.cache.model.CachedResource

@Dao
abstract class CachedResourceDao {

    @Query(ResourcesConstants.QUERY_RESOURCES)
    abstract fun getResources(): List<CachedResource>

    @Query(ResourcesConstants.DELETE_ALL_RESOURCES)
    abstract fun clearResources()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertResource(cachedResource: CachedResource)
}