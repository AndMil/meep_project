package com.meep.mobile.android.appname.datasources.resource.remote

import com.meep.mobile.android.appname.data.resource.source.ResourceRemoteDataSource
import com.meep.mobile.android.appname.datasources.remote.errorhandling.RemoteExceptionMapper
import com.meep.mobile.android.appname.datasources.resource.remote.mapper.ResourceRemoteMapper
import com.meep.mobile.android.appname.model.resource.LatLngModel
import com.meep.mobile.android.appname.model.resource.Resource
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Remote implementation for retrieving Resource instances. This class implements the
 * [ResourceRemote] from the Data layer as it is that layers responsibility for defining the
 * operations in which data store implementation layers can carry out.
 */
class ResourceRemoteImpl constructor(
        private val resourceService: ResourceService,
        private val resourceRemoteMapper: ResourceRemoteMapper
) : ResourceRemoteDataSource {
    
    override fun signOut(): Completable {
        return Completable.defer {
            Completable.complete()
        }
    }

    override fun getResources(lowerLeftLatLongModel: LatLngModel, upperRightLatLngModel: LatLngModel): Single<List<Resource>> {
        return resourceService.getResources(lowerLeftLatLongModel, upperRightLatLngModel).onErrorResumeNext{ throwable ->
            Single.error(RemoteExceptionMapper.getException(throwable))
        }.map { resources ->
            resources.map {
                resourceRemoteMapper.mapFromRemote(it)
            }
        }
    }
}