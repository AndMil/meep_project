package com.meep.mobile.android.appname.data.resource.source

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ResourceDataStoreFactoryTest {

    private val resourceCacheDataStore = mock<ResourceCacheDataSource>()
    private val resourceRemoteDataStore = mock<ResourceRemoteDataSource>()

    private val resourceDataStoreFactory = ResourceDataStoreFactory(
        resourceCacheDataStore, resourceRemoteDataStore
    )

    //<editor-fold desc="Retrieve Data Store">
    @Test
    fun retrieveDataStoreWhenNotCachedReturnsRemoteDataStore() {
        stubresourceCacheIsValidCache(Single.just(false))
        val resourceDataStore = resourceDataStoreFactory.retrieveDataStore(false)
        assertTrue(resourceDataStore == resourceRemoteDataStore)
    }

    @Test
    fun retrieveDataStoreReturnsCacheDataStore() {
        stubresourceCacheIsValidCache(Single.just(true))
        val resourceDataStore = resourceDataStoreFactory.retrieveDataStore(true)
        assertTrue(resourceDataStore == resourceCacheDataStore)
    }
    //</editor-fold>

    //<editor-fold desc="Retrieve Remote Data Store">
    @Test
    fun retrieveRemoteDataStoreReturnsRemoteDataStore() {
        val resourceDataStore = resourceDataStoreFactory.retrieveRemoteDataStore()
        assertTrue(resourceDataStore == resourceRemoteDataStore)
    }
    //</editor-fold>

    //<editor-fold desc="Retrieve Cache Data Store">
    @Test
    fun retrieveCacheDataStoreReturnsCacheDataStore() {
        val resourceDataStore = resourceDataStoreFactory.retrieveCacheDataStore()
        assertTrue(resourceDataStore == resourceCacheDataStore)
    }
    //</editor-fold>

    //<editor-fold desc="Stub helper methods">
    private fun stubresourceCacheIsValidCache(single: Single<Boolean>) {
        whenever(resourceCacheDataStore.isValidCache())
            .thenReturn(single)
    }
    //</editor-fold>

}