package com.meep.mobile.android.appname.data.resource.repository

import com.meep.mobile.android.appname.data.resource.source.ResourceCacheDataSource
import com.meep.mobile.android.appname.data.resource.source.ResourceDataSource
import com.meep.mobile.android.appname.data.resource.source.ResourceDataStoreFactory
import com.meep.mobile.android.appname.data.resource.test.factory.ResourceFactory
import com.meep.mobile.android.appname.data.resource.test.factory.ResourceFactory.makeLatLngModel
import com.meep.mobile.android.appname.model.resource.Resource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
open class ResourceDataRepositoryTest {

    private val resourceDataStoreFactory = mock<ResourceDataStoreFactory>()
    private val resourceCacheDataStore = mock<ResourceCacheDataSource>()

    private val resourceDataRepository = ResourceDataRepository(resourceDataStoreFactory)
    val mockLatLongLeft = makeLatLngModel()
    val mockLatLongRight = makeLatLngModel()

    @Before
    fun setUp() {
        stubResourceDataStoreFactoryRetrieveCacheDataStore()
    }

    //<editor-fold desc="Clear Resource">
    @Test
    fun clearResourceCompletes() {
        stubResourceCacheClearResources(Completable.complete())
        val testObserver = resourceDataRepository.clearResources().test()
        testObserver.assertComplete()
    }

    @Test
    fun clearResourceCallsCacheDataStore() {
        stubResourceCacheClearResources(Completable.complete())
        resourceDataRepository.clearResources().test()
        verify(resourceCacheDataStore).clearResources()
    }
    //</editor-fold>

    //<editor-fold desc="Save Resources">
    @Test
    fun saveResourcesCompletes() {
        stubResourceCacheSaveResources(Completable.complete())
        val testObserver = resourceDataRepository.saveResources(
                ResourceFactory.makeResourceList(2)
        ).test()
        testObserver.assertComplete()
    }

    @Test
    fun saveResourcesCallsCacheDataStore() {
        stubResourceCacheSaveResources(Completable.complete())
        resourceDataRepository.saveResources(ResourceFactory.makeResourceList(2)).test()
        verify(resourceCacheDataStore).saveResources(any())
    }
    //</editor-fold>

    //<editor-fold desc="Get Resources">

    @Test
    fun getResourcesReturnsData() {
        stubResourceCacheDataStoreIsCached(Single.just(true))
        stubResourceDataStoreFactoryRetrieveDataStore(resourceCacheDataStore)
        stubResourceCacheSaveResources(Completable.complete())
        val resources = ResourceFactory.makeResourceList(2)
        stubResourceCacheDataStoreGetResources(Single.just(resources))

        val testObserver = resourceDataRepository.getResources(mockLatLongLeft, mockLatLongRight).test()
        testObserver.assertValue(resources)
    }

    @Test
    fun getResourcesSavesResourcesWhenFromCacheDataStore() {
        stubResourceDataStoreFactoryRetrieveDataStore(resourceCacheDataStore)
        stubResourceCacheSaveResources(Completable.complete())
        resourceDataRepository.saveResources(ResourceFactory.makeResourceList(2)).test()
        verify(resourceCacheDataStore).saveResources(any())
    }
    //</editor-fold>

    //<editor-fold desc="Stub helper methods">
    private fun stubResourceCacheSaveResources(completable: Completable) {
        whenever(resourceCacheDataStore.saveResources(any()))
                .thenReturn(completable)
    }

    private fun stubResourceCacheDataStoreIsCached(single: Single<Boolean>) {
        whenever(resourceCacheDataStore.isValidCache())
                .thenReturn(single)
    }

    private fun stubResourceCacheDataStoreGetResources(single: Single<List<Resource>>) {
        whenever(resourceCacheDataStore.getResources(mockLatLongLeft, mockLatLongRight))
                .thenReturn(single)
    }

    private fun stubResourceCacheClearResources(completable: Completable) {
        whenever(resourceCacheDataStore.clearResources())
                .thenReturn(completable)
    }

    private fun stubResourceDataStoreFactoryRetrieveCacheDataStore() {
        whenever(resourceDataStoreFactory.retrieveCacheDataStore())
                .thenReturn(resourceCacheDataStore)
    }

    private fun stubResourceDataStoreFactoryRetrieveDataStore(dataSource: ResourceDataSource) {
        whenever(resourceDataStoreFactory.retrieveDataStore(any()))
                .thenReturn(dataSource)
    }
    //</editor-fold>

}