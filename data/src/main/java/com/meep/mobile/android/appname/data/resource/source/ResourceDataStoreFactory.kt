package com.meep.mobile.android.appname.data.resource.source

/**
 * Create an instance of a resourceDataStore
 */
open class ResourceDataStoreFactory(
        private val resourceCacheDataStore: ResourceCacheDataSource,
        private val resourceRemoteDataStore: ResourceRemoteDataSource
) {

    /**
     * Returns a DataStore based on whether or not there is content in the cache and the cache
     * has not expired
     */
    open fun retrieveDataStore(isValidCache: Boolean): ResourceDataSource {
        if (isValidCache) {
            return retrieveCacheDataStore()
        }
        return retrieveRemoteDataStore()
    }

    /**
     * Return an instance of the Cache Data Store
     */
    open fun retrieveCacheDataStore(): ResourceCacheDataSource {
        return resourceCacheDataStore
    }

    /**
     * Return an instance of the Remote Data Store
     */
    open fun retrieveRemoteDataStore(): ResourceRemoteDataSource {
        return resourceRemoteDataStore
    }
}