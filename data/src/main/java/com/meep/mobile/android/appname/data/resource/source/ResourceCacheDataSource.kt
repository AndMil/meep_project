package com.meep.mobile.android.appname.data.resource.source

import com.meep.mobile.android.appname.model.resource.Resource
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Interface defining methods for cache data operations related to resources.
 * This is to be implemented by external data source layers, setting the requirements for the
 * operations that need to be implemented.
 */
interface ResourceCacheDataSource : ResourceDataSource {

    // region resources
    fun saveResources(resources: List<Resource>): Completable

    fun clearResources(): Completable

    fun isValidCache(): Single<Boolean>

    fun setLastCacheTime(lastCache: Long)
    // endregion
}