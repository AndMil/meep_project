package com.meep.mobile.android.appname.data.resource.repository

import com.meep.mobile.android.appname.data.resource.source.ResourceDataStoreFactory
import com.meep.mobile.android.appname.domain.resource.repository.ResourceRepository
import com.meep.mobile.android.appname.model.resource.LatLngModel
import com.meep.mobile.android.appname.model.resource.Resource
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Provides an implementation of the [ResourceRepository] interface for communicating to and from
 * data sources
 */
class ResourceDataRepository(private val factory: ResourceDataStoreFactory) : ResourceRepository {

    // region resources
    override fun getResources(lowerLeftLatLongModel: LatLngModel, upperRightLatLngModel: LatLngModel): Single<List<Resource>> {
        return factory.retrieveCacheDataStore().isValidCache()
            .flatMap { cached ->
                // Get data store based on whether cached data is valid
                val resourceDataStore = factory.retrieveDataStore(cached)

                val resourceListSource = if (cached) {
                    // Getting data from cache
                    resourceDataStore.getResources(lowerLeftLatLongModel, upperRightLatLngModel)
                } else {
                    // Getting data from remote, so result is cached
                    resourceDataStore.getResources(lowerLeftLatLongModel, upperRightLatLngModel)
                        .flatMap { resourceList ->
                            // Once the result have been retrieved, save it to cache and return it
                            saveResources(resourceList).toSingle { resourceList }
                        }
                }

                resourceListSource
            }
    }

    fun saveResources(resources: List<Resource>): Completable {
        return factory.retrieveCacheDataStore().saveResources(resources)
    }

    fun clearResources(): Completable {
        return factory.retrieveCacheDataStore().clearResources()
    }
    // endregion
}