package com.meep.mobile.android.appname.data.resource.source

import com.meep.mobile.android.appname.model.resource.LatLngModel
import com.meep.mobile.android.appname.model.resource.Resource
import io.reactivex.Single

interface ResourceDataSource {

    fun getResources(lowerLeftLatLongModel: LatLngModel, upperRightLatLngModel: LatLngModel): Single<List<Resource>>
}