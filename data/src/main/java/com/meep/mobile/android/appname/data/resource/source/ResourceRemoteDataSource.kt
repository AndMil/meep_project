package com.meep.mobile.android.appname.data.resource.source

import io.reactivex.Completable

/**
 * Interface defining methods for remote data operations related to resources.
 * This is to be implemented by external data source layers, setting the requirements for the
 * operations that need to be implemented.
 */
interface ResourceRemoteDataSource : ResourceDataSource {

    // region Session
    fun signOut(): Completable
    // endregion
}