package com.meep.mobile.android.appname.model.resource

data class Resource(
        val id: String,
        val name: String,
        val x: Double,
        val y: Double,
        val scheduledArrival: Double,
        val locationType: Int,
        val companyZoneId: Int,
        val lat: Double,
        val lon: Double)