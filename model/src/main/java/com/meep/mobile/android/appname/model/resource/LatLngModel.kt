package com.meep.mobile.android.appname.model.resource

data class LatLngModel(
        val lat: Double,
        val lng: Double
) {
    override fun toString(): String {
        return String.format("%.6f,%.6f", lat, lng)
    }
}